#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]

fn main() {
    try_tuples::main();
}

mod try_tuples {
    pub fn main() {
        let rect = Rectangle {
            width: 30,
            height: 50,
        };
        println!("The area of the rectangle is {}", rect.area());
        println!("The rect: {:#?}", rect);
    }

    #[derive(Debug)]
    struct Rectangle {
        width: u32,
        height: u32,
    }

    impl Rectangle {
        pub fn area(&self) -> u32 {
            self.width * self.height
        }

        pub fn can_hold(&self, other: &Rectangle) -> bool {
            self.width > other.width && self.height > other.height
        }
    }

    impl Rectangle {
        fn square(size: u32) -> Rectangle {
            Rectangle {
                width: size,
                height: size,
            }
        }
    }
}

mod intro {
    struct User {
        username: String,
        email: String,
        sign_in_count: u64,
        active: bool,
    }

    fn build_users() {
        let mut user1 = User {
            email: String::from("bodgan@gmail.com"),
            username: String::from("bodgand123"),
            active: true,
            sign_in_count: 1,
        };

        let name: String = user1.username;
        user1.username = String::from("wallacel123");

        let user2 = build_user(String::from("kyle@mail.com"), String::from("kyl2123"));
        let user3 = User {
            email: String::from("james@gmail.com"),
            username: String::from("james123"),
            ..user2
        };
    }

    fn build_user(email: String, username: String) -> User {
        User {
            email,
            username,
            active: true,
            sign_in_count: 1,
        }
    }
}
